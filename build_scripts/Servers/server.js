const
	path = require('path'),
	express = require('express'),
	favicon = require('serve-favicon'),
	open = require('open'),
	webpack = require('webpack'),
	webpack_config = require('../../webpack.config.dev')
	bodyParser = require('body-parser');

	app = express(),
	port = 3000,
	compiler = webpack(webpack_config);

app.use(favicon(path.join(__dirname,'../../src/assets/favicon.png')));
app.use(require('webpack-dev-middleware')(compiler,{
	publicPath: webpack_config.output.publicPath
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

var posts = [
	{
		id: 0,
		username: 'Vincent',
		date:     new Date(),
		content:  'succ',
		likes:    666,
		isLiked: false,
		comments: [
			{
				username:'Coraline',
				content: 'good stuff',
				date:    new Date(),
			},
			{
				username:'Murray',
				content: 'dskjie',
				date:    'Monday at 12',
			},
		],
	},
	{
		id: 1,
		username: 'Morgaine',
		date:     'yesterday',
		content:  'loremipsumloremipsum',
		likes:    624,
		isLiked:  22,
		comments: [

		],
	},
];

var users = [
	{
		username: 'Vincent',
		about: 'i am cool',
		location: 'Toronto',
	},
	{
		username: 'Morgaine',
		about: 'i am not cool',
		location: 'Toronto',
	},
	{
		username: 'Angeline',
		about: 'i am loaf',
		location: 'Toronto',
	},
	{
		username: 'Coraline',
		about: 'meow',
		location: 'the vent',
	},
	{
		username: 'Twinkey',
		about: 'i am talkative',
		location: 'under the bed',
	},
	{
		username: 'Murray',
		about: 'i am affectionate',
		location: 'outside',
	},
];

app.get('/api/posts', (req, res) => {
	res.json(posts);
});

app.get('/api/users', (req, res) => {
	res.json(users);
});

app.get('/api/users/:username', (req, res) => {
	let username = req.params.username;
	let user = users.find(x => x.username.toLowerCase() === username);

	if (user == null) {
		res.status(400).send('That user does not exist');
		res.end();
	}

	res.json(user);
});

app.post('/api/posts/:id/comments', (req, res) => {
	let id = req.params.id;
	let post = posts.find(x => x.id == id);

	if (!post) {
		res.status(500).send('no post with that id found');
		res.end();
	}

	post.comments.push(req.body);
	res.json(post);
});

app.get('/*',(req,res) => res.sendFile(path.join(__dirname,'../../src/index.html')));


app.listen(port, err => err ? console.log(err) : open(`http://localhost:${port}`));