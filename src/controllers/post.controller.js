angular.module('app')

.controller('PostCtrl', ['$scope', 'PostSvc', function ($scope, postSvc) {
    var data = $scope.$parent.$ctrl.data;

    $scope.likePost = () => {
        if (data.isLiked) {
            data.likes--;
        } else {
            data.likes++;
        }
        data.isLiked = !data.isLiked;
        $scope.setLikeColor();
    };

    $scope.setLikeColor = () => {
        data.likeColor = data.isLiked ? { 'color': 'red'} 
            : { 'color' : 'grey' }
    };

    $scope.postComment = () => {
        console.log(data);

        let postPromise = postSvc.posts.postComment(data.id, {
            content: $scope.comment,
            date: new Date(),
            username: 'Vincent',
        });

        postPromise.then((res) => {
            data.comments = res.data.comments;
        }, err => {
            console.log('uh oh');
        });
        
        $scope.comment = "";
    };

}]);