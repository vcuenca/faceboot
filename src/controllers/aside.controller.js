angular.module('app')

.controller('AsideCtrl', ['$scope', 'PostSvc', '$state', function ($scope, postSvc, $state) {

    let userPromise = postSvc.users.getAll();
    userPromise.then((res) => {
        $scope.users = res.data;
    }, (err) => {
        console.log('Something bad happened: ' + err.statusText);
    });

    $scope.gotoProfile = username => {
        $state.go('profile', {username: username});
    }
    
}]);