angular.module('app')

.controller('PostListCtrl', ['$scope', 'PostSvc', function ($scope, postSvc) {
    

    let postPromise = postSvc.posts.getAll();

    postPromise.then(res => {
        $scope.posts = res.data;
    }, err => {
        alert('something broke');
    })    
}]);