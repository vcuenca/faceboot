angular.module('app')

.config($stateProvider => {
    $stateProvider

    .state('root', {
        url: '/',
        component: 'root'
    })

    .state('profile', {
        url: '/profile/:username',
        component: 'profile',
    });
})

.component('root', {
    template: `
    <body>
        <navbar username="Vincent"></navbar>
        <div class="container" ng-controller="PostListCtrl">
            <div class="content">
                <post data="post" ng-repeat="post in posts"></post>
            </div>
            <aside></aside>
        </div>
        <footer></footer>
    </body>
    `
});