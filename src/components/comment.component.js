angular.module('app')


.component('comment', {
    bindings: {
        data: '=',
    },
    
    template: `
    <li class="flex-col comment-list-item">
        <!--USER COMMENT HEADER-->
        <div class="flex-between">
            <a class="profile profile--hover-ul"
            ui-sref="profile({username: $ctrl.data.username})">
                <img src="./assets/stock_profile.jpg">
                <span> {{ $ctrl.data.username }}</span>
            </a>
            <span class="timestamp text-sm">{{ $ctrl.data.date | date: 'LLLL d @ HH:mm' }}</span>
        </div>
        <!--USER COMMENT CONTENT-->
        <p class="text-md margin-8-top">
            {{ $ctrl.data.content }}
        </p>
    </li>
    `
});