angular.module('app')

//May 30 at 9:59pm
 
.component('post', {
    bindings: {
        data: '=',
    },
    template: `
        <div class="post" ng-controller="PostCtrl">
            <!--USER POST HEADER-->
            <div class="post__header flex-between margin-16">
                <a class="profile profile--lg profile--hover-ul" href="#"
                ui-sref="profile({username: $ctrl.data.username})">
                    <img class="profile-pic" src="./assets/stock_profile.jpg">
                    <span>{{ $ctrl.data.username }}</span>
                </a>
                <span class="timestamp"> {{ $ctrl.data.date | date: 'LLLL d @ HH:mm'  }}</span>
            </div>
            <!--USER POST CONTENT-->
            <div class="text-md margin-16">
                {{ $ctrl.data.content }}
            </div>
            <!--USER POST FOOTER-->
            <div class="flex-between margin-16 count-bar">
                <div class="flex">
                    <a ng-click="likePost()" ng-style="$ctrl.data.likeColor" class="btn-icon btn--hover-red" href="#">
                        <i class="fa fa-heart fa-lg"></i>
                    </a>
                    <span> {{ $ctrl.data.likes }}</span>
                </div>
                <div class="flex">
                    <a class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a>
                    <span class="replyCount"> {{ $ctrl.data.comments.length }} </span>
                </div>
            </div>
            <ul className="margin-16 top-divider">
                <div className="comments">
                    <comment data="comment" ng-repeat="comment in $ctrl.data.comments"></comments>
                </div>
                <div class="p-input">
                    <input ng-model="comment" contentEditable="true" class="p-input__ta" placeholder="Add a comment..."></input>
                    <a ng-click="postComment()" class="btn btn-primary f-right margin-8-v" href="#">POST</a>
                </div>
            </ul>
        </div>
    `,
});