angular.module('app')

.component('aside', {
    template: `
    <div ng-controller="AsideCtrl">
        <div class="aside">Users</div>
        <div class="user-aside profile profile--hover-bg" 
            data="user" ng-repeat="user in users"
            ng-click="gotoProfile(user.username)">
            {{ user.username }}
        </div>
    </div>
    `,
})