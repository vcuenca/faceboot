angular.module('app')

.component('profile', {
    template: `
    <body>
        <navbar username="Vincent"></navbar>
        <div class="container profile-container">
            <h2>{{ user.username }}</h2>
            <p>{{ user.about }}</p>
            <p>Location: {{ user.location }}</p>        
        </div>
    </body>
    `,
    controller: ['$scope', '$stateParams', '$state', 'PostSvc', 
            function ($scope, $stateParams, $state, PostSvc) {
        $scope.user = {};

        let username = $stateParams.username;
        let promise = PostSvc.users.getUser(username);

        promise.then(res => {
            $scope.user = res.data;
        }, err => {
            alert('That user doesn\'t exist! Returning you to the home page..');
            $state.go('root');
        });        
    }], 
    
});