angular.module('app')

.component('navbar', {
    bindings: {
        username: '@'
    },
    template: `
    <header class="page-header">
        <!--navbar block-->
        <div class="navbar">
            <a class="logo" ui-sref="root"></a>
            <ul class="nav-h">
                <li>
                    <a id="profile-button" class="btn profile profile--hover-bg"
                        ui-sref="profile({username: $ctrl.username })">
                        <img src="../assets/profile.jpg">
                        <span>{{ $ctrl.username }}</span>
                    </a>
                </li>
                <li>
                    <a id="logout" class="btn" href="#">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        <span>
                            Log Out
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </header>
    `
});