require('./assets/styles/index.css');

require('angular');
require('angular-ui-router');


angular.module('app', ['ui.router'])

.config(($stateProvider, $locationProvider, $urlRouterProvider) => {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/'); // if unknown, goto this route
});

require('./services/post.service.js');

const context = require.context('.',true,/\.component\.js$/);
context.keys().forEach(context);

require('./controllers/post.controller.js');
require('./controllers/postList.controller.js');
require('./controllers/aside.controller.js');

console.log('I AM ALIVE');