angular.module('app')

.service('PostSvc', function ($http) {

    return {
        posts: {
            getAll: () => {
                return $http.get('/api/posts');
            },

            postComment: (id, comment) => {
                return $http.post('/api/posts/' + id + '/comments',
                    comment);
            },
        },
        users: {
            getAll: () => {
                return $http.get('/api/users');
            },
            getUser: username => {
                return $http.get('/api/users/' + username.toLowerCase());
            },
        },
    }

});